const fs = require('fs-extra')
const path = require('path')
const ParcelBundler = require('parcel')

const mountPackage = process.argv[2]

if (typeof mountPackage !== 'string') {
  throw new Error(
    'Syntax: package=react/mount npm run local:build -- <package to build>'
  )
}

const modules = path.join(__dirname, 'modules')
const dir = path.join(modules, mountPackage.replace('/', '---'))
const outDir = path.join(dir, 'dist')
const staticDir = path.join(__dirname, '..', '..', mountPackage, 'static')

try {
  fs.mkdirSync(modules)
} catch (e) {}

try {
  fs.mkdirSync(dir)
} catch (e) {}

const html = fs
  .readFileSync(path.join(__dirname, 'index.html'))
  .toString()
  .replace(/\$package/g, mountPackage)

const indexFile = path.join(dir, 'index.html')

fs.writeFileSync(indexFile, html)

const bundler = new ParcelBundler(indexFile, {
  autoinstall: false,
  outDir
})

bundler.on('buildEnd', () => {
  if (fs.existsSync(staticDir)) {
    fs.copySync(staticDir, outDir, { recursive: true })
  }
})

bundler.bundle()
