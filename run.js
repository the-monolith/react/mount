const fs = require('fs')
const path = require('path')
const ParcelBundler = require('parcel')

const mountPackage = process.argv[2]

if (typeof mountPackage !== 'string') {
  throw new Error(
    'Syntax: package=react/mount npm run local:run -- <package to run mount for>'
  )
}

const modules = path.join(__dirname, 'modules')
const dir = path.join(modules, mountPackage.replace('/', '---'))

try {
  fs.mkdirSync(modules)
} catch (e) {}

try {
  fs.mkdirSync(dir)
} catch (e) {}

const html = fs
  .readFileSync(path.join(__dirname, 'index.html'))
  .toString()
  .replace(/\$package/g, mountPackage)

const indexFile = path.join(dir, 'index.html')

fs.writeFileSync(indexFile, html)

const bundler = new ParcelBundler(indexFile, {
  autoinstall: false,
  outDir: path.join(dir, 'dist')
})

bundler.serve(process.env.PORT || 5999)
