export const React: {} = (global as any).React

import { Renderer } from 'react-dom'

const globalReactDOM = (window as any).ReactDOM

const renderDOM: Renderer = (typeof globalReactDOM !== 'undefined'
  ? globalReactDOM
  : {
      render: () => {
        /* void */
      }
    }
).render

const getOrCreateElement = (id: string) => {
  const existingElement: HTMLDivElement | null = document.getElementById(
    id
  ) as HTMLDivElement | null

  if (existingElement !== null) {
    return existingElement
  }

  const newElement = document.createElement('div')
  newElement.setAttribute('id', id)
  document.body.appendChild(newElement)

  return newElement
}

export const mount = (
  element: JSX.Element,
  containerId: string = 'default'
) => {
  renderDOM(element, getOrCreateElement(containerId))
}
